<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function products()
    {
        return $this->belongsToMany(Product::class, 'order_products')->withPivot('quantity');
    }

    public function productCount(): int
    {
        return $this->products->count();
    }

    public function total(): int
    {
        $total = 0;

        foreach ($this->products as $product) {
            $total += ($product->price * $product->pivot->quantity);
        }

        return $total;
    }

    public function approve()
    {
        $this->is_pending = 0;
        $this->save();
    }
}

<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CheckoutController extends Controller
{
    /**
     * OrderController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $products = Auth::user()->basketProducts;

        $total = 0;
        foreach ($products as $product) {
            $total += $product->pivot->quantity * $product->price;
        }

        return view('page.checkout', [
            'products' => $products,
            'total' => $total
        ]);
    }

    public function create(Request $request)
    {
        $toSync = Auth::user()->basketProducts->pluck('pivot.quantity', 'id')
            ->map(function ($toSync) {
                return [
                    'quantity' => $toSync
                ];
            });

        $order = new Order();
        $order->user()->associate(Auth::user());
        $order->address = $request->post('address');
        $order->save();

        $order->products()->sync($toSync);

        Auth::user()->basketProducts()->sync([]);

        return redirect(route('home'))->with('success', 'Your order has placed');
    }
}

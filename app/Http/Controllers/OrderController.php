<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    /**
     * OrderController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $orders = Auth::user()->orders;

        return view('page.order-list', [
            'orders' => $orders,
        ]);
    }

    public function detail(Order $order, Request $request)
    {
        if (Auth::id() !== $order->user_id) {
            return redirect('home')->with('error', 'Order not found');
        }

        return view('page.order-detail', [
            'order' => $order,
            'products' => $order->products,
            'total' => $order->total()
        ]);
    }
}

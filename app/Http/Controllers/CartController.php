<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{
    public function index(Request $request)
    {
        if (!Auth::user()) {
            return redirect()->route('home')
                ->with('warning', 'You are not logged in!');
        }

        $products = Auth::user()->basketProducts;

        $total = 0;
        foreach ($products as $product) {
            $total += $product->pivot->quantity * $product->price;
        }

        return view('page.cart', [
            'products' => $products,
            'total' => $total
        ]);
    }
}

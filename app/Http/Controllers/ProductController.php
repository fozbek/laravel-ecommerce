<?php

namespace App\Http\Controllers;

use App\Product;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    public function index(Product $product, Request $request)
    {
        return view('page.product-detail', [
            'product' => $product
        ]);
    }

    public function add(Product $product, Request $request)
    {
        if (!Auth::id()) {
            return back()->with('error', 'You are not logged in!');
        }

        $quantity = $request->post('quantity', 1);

        Auth::user()->basketProducts()->sync([
            $product->id => [
                'quantity' => $quantity
            ]
        ], false);

        return back()->with('info', 'Product added!');
    }

    public function remove(Product $product)
    {
        if (!Auth::id()) {
            return back()->with('error', 'You are not logged in!');
        }

        Auth::user()->basketProducts()->detach($product->id);

        return back()->with('info', 'Product removed!');
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function index()
    {
        return view('admin.page.order-list', [
            'orders' => Order::with('user')->paginate(10),
        ]);
    }

    public function detail(Order $order, Request $request)
    {
        return view('admin.page.order-detail', [
            'order' => $order->load('user'),
            'products' => $order->products,
            'total' => $order->total()
        ]);
    }

    public function approve(Order $order, Request $request)
    {
        $order->approve();

        return back()->with('success', 'Order has approved');
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index()
    {
        return view('admin.page.product-list', [
            'products' => Product::paginate(15),
        ]);
    }

    public function create(Request $request)
    {
        return view('admin.page.product-create');
    }

    public function save(Request $request)
    {
        $productData = $request->validate([
            'label' => 'required',
            'description' => 'required',
            'sku' => 'required',
            'image' => 'required',
            'price' => 'required|numeric'
        ]);

        $product = new Product($productData);
        $product->save();

        return redirect(route('admin-product-list'))->with('success', 'Product created');
    }

    public function remove(Product $product)
    {
        $product->delete();

        return redirect(route('admin-product-list'))->with('success', 'Product removed');
    }
}

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::get('/product/{product}', 'ProductController@index')->name('product-detail');
Route::post('/product/{product}', 'ProductController@add')->name('add-product');

Route::get('/cart', 'CartController@index')->name('cart');
Route::get('/product/remove/{product}', 'ProductController@remove')->name('remove-product');

Route::get('/checkout', 'CheckoutController@index')->name('checkout');
Route::post('/checkout', 'CheckoutController@create')->name('checkout-create');

Route::get('/order', 'OrderController@index')->name('order-list');
Route::get('/order/{order}', 'OrderController@detail')->name('order-detail');

Route::middleware('admin')->prefix('/admin')->group(function () {
    Route::get('/', 'Admin\DashboardController@index')->name('admin-dashboard');

    Route::get('/order', 'Admin\OrderController@index')->name('admin-order-list');
    Route::get('/order/{order}', 'Admin\OrderController@detail')->name('admin-order-detail');
    Route::get('/order/approve/{order}', 'Admin\OrderController@approve')->name('admin-order-approve');

    Route::get('/product', 'Admin\ProductController@index')->name('admin-product-list');
    Route::get('/product/remove/{product}', 'Admin\ProductController@remove')->name('admin-product-remove');
    Route::get('/product/create', 'Admin\ProductController@create')->name('admin-product-create');
    Route::post('/product/create', 'Admin\ProductController@save')->name('admin-product-save');
});

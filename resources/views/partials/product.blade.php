<div class="col-md-3 mb-4">
    <div class="card">
        <div class="card-header">
            <a href="{{ route('product-detail', $product->id) }}">
                {{ $product->label }}
            </a>
        </div>
        <div class="card-body">
            <a href="{{ route('product-detail', $product->id) }}">
                <img src="{{ $product->image }}" alt="none" style="width: 100%">
            </a>
        </div>

        <div class="card-footer">
            <div class="row">
                <div class="col-md-6">{{ $product->price }} TRY</div>
                <div class="col-md-6">
                    <div class="text-right">
                        <a href="{{ route('product-detail', $product->id) }}" class="btn btn-primary pull-right">View</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

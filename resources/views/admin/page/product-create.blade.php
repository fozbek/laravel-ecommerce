@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-9">
            <div class="card">
                <div class="card-header">
                    <b>New Product</b>
                </div>
                <div class="card-body">
                    <form action="{{ route('admin-product-save') }}" method="post">
                        @csrf

                        <div class="form-group">
                            <label for="label">Label</label>
                            <input type="text" name="label" class="form-control" id="label" placeholder="Label">
                        </div>
                        <div class="form-group">
                            <label for="sku">SKU</label>
                            <input type="text" name="sku" class="form-control" id="sku" placeholder="SKU">
                        </div>
                        <div class="form-group">
                            <label for="image">Image (url)</label>
                            <input type="text" name="image" class="form-control" id="image" placeholder="Image">
                        </div>
                        <div class="form-group">
                            <label for="price">Price (TRY)</label>
                            <input type="number" required name="price" class="form-control" id="price" placeholder="Price">
                        </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <input type="text" name="description" class="form-control" id="description" placeholder="Description">
                        </div>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </form>
                </div>
            </div>
        </div>

    </div>
@endsection

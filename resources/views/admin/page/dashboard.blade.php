@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-3">
            <a href="{{ route('admin-order-list') }}" class="btn btn-primary btn-block">Orders</a>
            <a href="{{ route('admin-product-list') }}" class="btn btn-primary btn-block">Products</a>
        </div>
    </div>
@endsection

@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-9">
            <div class="card">
                <div class="card-header">
                    Order Number <b>#{{ $order->id }}</b>
                </div>
                <div class="card-body">
                    <ul>
                        <li>Address: <b>{{ $order->address }}</b></li>
                        <li>Method: <b>Cash On Delivery</b></li>
                        <li>Status: <b>{{ $order->is_pending ? 'Pending' : 'Sent' }}</b></li>
                        <li>Order Date: <b>{{ $order->created_at }}</b></li>
                        <li>User Name: <b>{{ $order->user->name }}</b></li>
                        <li>User Email: <b>{{ $order->user->email }}</b></li>
                    </ul>
                </div>
                <div class="col-md-12">
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Label</th>
                            <th scope="col">Quantity</th>
                            <th scope="col">Price</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($products as $product)
                            <tr>
                                <th scope="row">{{ $product->id }}</th>
                                <td>{{ $product->label }}</td>
                                <td>{{ $product->pivot->quantity }}</td>
                                <td>{{ $product->price * $product->pivot->quantity }} TRY</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>

                @if($order->is_pending)
                    <div class="col-md-12 mb-3">
                        <a href="{{ route('admin-order-approve', $order->id) }}" class="btn btn-block btn-success">Approve</a>
                    </div>
                @else
                    <div class="col-md-12 mb-3">
                        <a href="{{ route('admin-order-approve', $order->id) }}" class="btn btn-block btn-success disabled">Order is already approved</a>
                    </div>
                @endif

            </div>
        </div>
    </div>
@endsection

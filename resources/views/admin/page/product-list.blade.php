@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <h1>Products</h1>
        <div class="col-md-12">
            <a href="{{ route('admin-product-create') }}" class="btn btn-primary">New</a>
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Label</th>
                    <th scope="col">SKU</th>
                    <th scope="col">Image</th>
                    <th scope="col">Price</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>
                <tbody>

                @foreach($products as $product)
                    <tr>
                        <th scope="row">{{ $product->id }}</th>
                        <td>{{ $product->label }}</td>
                        <td>{{ $product->sku }}</td>
                        <td>{{ $product->image }}</td>
                        <td>{{ $product->price }} TRY</td>
                        <td><a href="{{ route('admin-product-remove', $product->id) }}"
                               class="btn btn-danger">Remove</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

    </div>
@endsection

@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <h1>Orders</h1>
        <div class="col-md-12">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Item Count</th>
                    <th scope="col">Total</th>
                    <th scope="col">Order Date</th>
                    <th scope="col">User Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Status</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($orders as $order)
                    <tr>
                        <th scope="row">{{ $order->id }}</th>
                        <td>{{ $order->productCount() }}</td>
                        <td>{{ $order->total() }} TRY</td>
                        <td>{{ $order->created_at }}</td>
                        <td>{{ $order->user->name }}</td>
                        <td>{{ $order->user->email }}</td>
                        <td>{{ $order->is_pending ? 'Pending' : 'Sent' }}</td>
                        <td><a href="{{ route('admin-order-detail', $order->id) }}"
                               class="btn btn-primary">Detail</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

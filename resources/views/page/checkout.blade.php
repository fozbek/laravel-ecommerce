@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-9">
            <form action="{{ route('checkout-create') }}" method="post">
                @csrf

                <div class="card">
                    <div class="card-header"><b>Checkout</b></div>

                    <div class="card-body">
                        <div class="products col-md-12">
                            Products:
                            <ul>
                                @foreach($products as $product)
                                    <li>
                                        {{ $product->label }} x{{ $product->pivot->quantity }} {{ $product->price }} TRY
                                    </li>
                                @endforeach
                            </ul>
                        </div>

                        <div class="total col-md-12">
                            Total: {{ $total }} TRY
                        </div>

                        <div class="col-md-12">
                            <div class="form-group mt-3">
                                <label for="exampleTextarea">Address</label>
                                <textarea class="form-control" required minlength="10" name="address" id="exampleTextarea" rows="3"></textarea>
                            </div>
                        </div>

                        <div class="col-md-12">
                            Payment Method
                            <div class="text-muted">Cash on delivery</div>
                        </div>
                    </div>

                    <div class="card-footer">
                        <input type="submit" class="btn btn-success btn-block" value="Checkout Now">
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

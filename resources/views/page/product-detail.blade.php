@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-3">
            <a href="{{ route('product-detail', $product->id) }}">
                <img src="{{ $product->image }}" alt="none" style="width: 100%">
            </a>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <a href="{{ route('product-detail', $product->id) }}">
                        {{ $product->label }}
                    </a>
                </div>

                <div class="card-body">

                    {{ $product->description }}

                </div>
                <div class="card-footer">
                    <div class="row">
                        <div class="col-md-6"><h1>{{ $product->price }} TRY</h1></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('add-product', $product->id) }}" method="post">
                        @csrf
                        <div class="form-group row">
                            <label for="example-number-input" class="col-12 col-form-label">Quantity</label>
                            <div class="col-12">
                                <input class="form-control" name="quantity" type="number" value="1" id="example-number-input">
                            </div>
                        </div>

                        <input type="submit" class="btn btn-primary btn-block pull-right" value="Add To Cart">
                    </form>
                </div>
            </div>
        </div>

    </div>
@endsection

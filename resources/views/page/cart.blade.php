@extends('layouts.app')

@section('content')
    @if($products->count())

        <div class="row justify-content-center">
            <div class="col-md-9">
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Label</th>
                        <th scope="col">Quantity</th>
                        <th scope="col">Price</th>
                        <th scope="col">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($products as $product)
                        <tr>
                            <th scope="row">{{ $product->id }}</th>
                            <td>{{ $product->label }}</td>
                            <td>{{ $product->pivot->quantity }}</td>
                            <td>{{ $product->price * $product->pivot->quantity }} TRY</td>
                            <td><a href="{{ route('remove-product', $product->id) }}"
                                   class="btn btn-danger">Remove</a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="col-md-3">
                <div class="card">
                    <div class="card-body">
                        Product Count: {{ $products->count() }}
                        <br>
                        Total: {{ $total }} TRY
                    </div>
                    <div class="card-footer">
                        <a href="{{ route('checkout') }}" class="btn btn-success btn-block">Checkout</a>
                    </div>
                </div>
            </div>
        </div>
    @else
        Your cart is empty
    @endif
@endsection

@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-9">
            <div class="card">
                <div class="card-header">
                    Order Number <b>#{{ $order->id }}</b>
                </div>
                <div class="card-body">
                    <ul>
                        <li>Address: {{ $order->address }}</li>
                        <li>Method: Cash On Delivery</li>
                        <li>Status: {{ $order->is_pending ? 'Pending' : 'Sent' }}</li>
                        <li>Order Date: {{ $order->created_at }}</li>
                    </ul>
                </div>
                <div class="col-md-12">
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Label</th>
                            <th scope="col">Quantity</th>
                            <th scope="col">Price</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($products as $product)
                            <tr>
                                <th scope="row">{{ $product->id }}</th>
                                <td>{{ $product->label }}</td>
                                <td>{{ $product->pivot->quantity }}</td>
                                <td>{{ $product->price * $product->pivot->quantity }} TRY</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">

        @foreach($products as $product)
            @include('partials.product', ['product' => $product])
        @endforeach

    </div>
@endsection

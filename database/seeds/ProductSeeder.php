<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        foreach (range(0, 10) as $th) {
            DB::table('products')->insert([
                'label' => $faker->text(50),
                'description' => $faker->text,
                'sku' => $faker->ean8,
                'image' => $faker->imageUrl(),
                'price' => $faker->numberBetween(100, 500),
                'stock_qty' => $faker->numberBetween(100, 500),
            ]);
        }

    }
}

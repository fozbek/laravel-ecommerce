<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Admin',
            'email' => 'info@fozbek.com',
            'password' => Hash::make('adminpassword'),
            'role' => 'ROLE_ADMIN',
        ]);
        DB::table('users')->insert([
            'name' => 'User',
            'email' => 'user@mail.com',
            'password' => Hash::make('userpassword'),
            'role' => 'ROLE_USER',
        ]);
    }
}
